<?php
    class FindPrime
    {
        public function find(int $start, int $end) : int
        {
            /* if end lowers than start - return zero */
            if ($start > $end) {
                return 0;
            }

            $numbers = range($start, $end); // create array from start and end numbers with range
            $center = (int) floor((sizeof($numbers) - 1) / 2); // get center index

            /* if center element is prime - return and stop */
            if ($this->isPrime($numbers[$center])) {
                return $numbers[$center];
            }

            return $this->findPrimeInArray($numbers, $center);
        }

        private function findPrimeInArray(array $array, int $center) : int
        {
            $l = 0;
            $r = 0;

            $lPrime = 0;
            $rPrime = 0;

            /* check numbers from center to first element and collect steps count */
            for ($i = $center; $i > 0; $i--) {
                if ($this->isPrime($array[$i])) {
                    $lPrime = $array[$i];
                    break;
                }

                $l++;
            }

            /* check numbers from center to last element and collect steps count */
            for ($i = $center; $i < sizeof($array); $i++) {
                if ($this->isPrime($array[$i])) {
                    $rPrime = $array[$i];
                    break;
                }

                $r++;
            }

            /* return number with lowest steps */
            return ($l < $r) ? $lPrime : $rPrime;
        }

        private function isPrime(int $i) : bool
        {
            $n = 2;
            while ($n < $i) {
                if ($i % $n) {
                    $n++;
                    continue;
                }

                return false;
            }

            return true;
        }
    }
